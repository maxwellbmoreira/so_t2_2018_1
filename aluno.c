#include "simplegrade.h"
#include "memvirt.h"


#pragma GCC diagnostic ignored "-Wwrite-strings"

void aluno1()
{
    struct result * res = NULL;
    int i;

    //printf("vai chamar a fun��o\n");
    res = memvirt(3,6,"alunos/t01.txt");
    //printf("saiu da funcao\n");

    if(res == NULL)
    {
        printf("FAIL!\n");
        return;
    }
    else
    {

    printf("Total de referencias por processo:\n");

    for(i=0;i<3;i++){
        printf("PID %d: %d\n",i,res->refs[i]);
    }
    printf("====\n");
    printf("Total de faults por processo:\n");
    for(i=0;i<3;i++){
        printf("PID %d: %d\n",i,res->pfs[i]);
    }
    printf("Taxa de faults por processo:\n");
    for(i=0;i<3;i++){
        printf("PID %d: %.2f%% \n",i,res->pf_rate[i]);
    }
    printf("Taxa total de faults: %.2f%%\n",res->total_pf_rate);
    }
}


void aluno2()
{

    struct result * res;
    int i;

    res = memvirt(4, 15, "alunos/t02.txt");

    if(res == NULL)
    {
        printf("FAIL!\n");
        return;
    }
    else
    {

    printf("Total de referencias por processo:\n");

    for(i=0;i<4;i++){
        printf("PID %d: %d\n",i,res->refs[i]);
    }
    printf("====\n");
    printf("Total de faults por processo:\n");
    for(i=0;i<4;i++){
        printf("PID %d: %d\n",i,res->pfs[i]);
    }
    printf("Taxa de faults por processo:\n");
    for(i=0;i<4;i++){
        printf("PID %d: %.2f%% \n",i,res->pf_rate[i]);
    }
    printf("Taxa total de faults: %.2f%%\n",res->total_pf_rate);
    }

}

void aluno3()
{
    struct result * res;
    int i;
    printf("ALUNO 3\n");
    res = memvirt(5, 10, "alunos/t03.txt");

   if(res == NULL)
    {
        printf("FAIL!\n");
        return;
    }
    else
    {

    printf("Total de referencias por processo:\n");

    for(i=0;i<5;i++){
        printf("PID %d: %d\n",i,res->refs[i]);
    }
    printf("====\n");
    printf("Total de faults por processo:\n");
    for(i=0;i<5;i++){
        printf("PID %d: %d\n",i,res->pfs[i]);
    }
    printf("Taxa de faults por processo:\n");
    for(i=0;i<5;i++){
        printf("PID %d: %.2f%% \n",i,res->pf_rate[i]);
    }
    printf("Taxa total de faults: %.2f%%\n",res->total_pf_rate);
    }



}


int main()
{
    aluno1();
    aluno2();
    aluno3();

    return 0;
}
