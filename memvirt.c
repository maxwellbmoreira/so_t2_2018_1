#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "memvirt.h"


typedef struct frame
{
    int frameID;
    int pageID;
    int ref;
    struct frame *prox;

} TFrame;

typedef struct process
{
    int usedFrames;
    TFrame *head;
} TProcess;

int buildProcessList(TProcess *processList[], int num_procs);
int buildFrameList(TProcess *processList[], int num_procs, int maxFrames);
int printMemory(TProcess *processList[], int num_procs, int maxFrames);
int startClockPointers(TFrame *clockPointer[], TProcess *processList[], int num_procs);
int seekPage(TProcess *processList[],int wantedProcess, int wantedPageID);
TFrame* insertOnMemory(TFrame *clockPointer,int wantedPageID);
void printResults(struct result * res,int numProcess);


struct result * memvirt(int num_procs, uint32_t num_frames, char * filename)
{

    if(num_procs<=0)
        return NULL;

    if(num_frames<=0)
        return NULL;

    int frames = (int)num_frames;

    if(frames<num_procs)
        return NULL;

    int maxFrames = num_frames / num_procs;

    if(0 > (int)(num_frames - num_procs))
        return NULL;

    //printf("MAX FRAMES: %d\n",maxFrames);
    struct result *resultados = (struct result*)malloc(sizeof(struct result));
    resultados->refs = (uint32_t*) malloc(num_procs*sizeof(uint32_t));
    resultados->pfs = (uint32_t*) malloc(num_procs*sizeof(uint32_t));
    resultados->pf_rate = (float*) malloc(num_procs*sizeof(float));


    uint32_t localRefs[num_procs];
    uint32_t localPfs[num_procs];
    float localFaultRate[num_procs];
    float totalPfRate=0;

    uint32_t totalRef=0;
    uint32_t totalFault=0;

    int i;
    for(i=0; i<num_procs; i++)
    {
        localFaultRate[i]=0;
        localRefs[i]=0;
        localPfs[i]=0;
    }


    //printf("Numero de quadros por processo: %d\n", maxFrames);


    FILE *arq;
    arq = fopen(filename, "rt");

    if (arq == NULL)
    {
        //printf("ERROR OPEN FILE\n");
        return NULL;
    }

    TProcess *processList[num_procs];

    buildProcessList(processList,num_procs);
    buildFrameList(processList,num_procs,maxFrames);
    //printMemory(processList,num_procs,maxFrames);

    TFrame *clockPointer[num_procs];
    startClockPointers(clockPointer,processList,num_procs);




    /*if(!(seekPage(processList[3],8))){
        printf("HIT");
    }
    else
    {
        printf("MISS");
    }*/
    int wantedProcess, wantedPage;

    while (!feof(arq))
    {

        fscanf(arq,"%d %d \n",&wantedProcess,&wantedPage);

        //printf("%d - %d \n", wantedProcess, wantedPage);

        localRefs[wantedProcess]++;
        totalRef++;
        //printf("%d\n", totalRef);

        if(!(seekPage(processList,wantedProcess,wantedPage)))
        {
            //printf("HIT\n");
        }
        else
        {
            //printf("MISS\n");
            localPfs[wantedProcess]++;
            totalFault++;
            clockPointer[wantedProcess]=insertOnMemory(clockPointer[wantedProcess],wantedPage);
        }

    }
    fclose(arq);
    //printMemory(processList,num_procs,maxFrames);

    for(i=0; i<num_procs; i++)
    {
        localFaultRate[i]=((float)localPfs[i]/localRefs[i])*100;
        //printf("%f\n",localFaultRate[i]);
    }

    totalPfRate=((float)totalFault/totalRef)*100;
    for(i=0; i<num_procs; i++)
    {
        resultados->refs[i]=localRefs[i];
        resultados->pfs[i]=localPfs[i];
        resultados->pf_rate[i]=localFaultRate[i];
    }
    resultados->total_pf_rate=totalPfRate;

    //printResults(resultados,num_procs);


    return resultados;
}


int buildProcessList(TProcess *processList[], int num_procs)
{
    //printf("buildProcessList\n");
    int i;
    TProcess *newProcess;

    for(i = 0; i < num_procs; i++)
    {
        newProcess = (TProcess*)malloc(sizeof(TProcess));
        newProcess->head=NULL;
        newProcess->usedFrames=0;
        processList[i]=newProcess;
    }

    //printf("vetor de processos criado com sucesso\n");
    return 0;
}

int buildFrameList(TProcess *processList[], int num_procs, int maxFrames)
{
    //printf("buildFrameList\n");
    TFrame *newFrame;
    struct frame *aux;
    int i,j;
    for (i = 0; i < num_procs; i++)
    {
        j=0;
        newFrame =(TFrame*)malloc(sizeof(TFrame));
        newFrame->frameID=j;
        newFrame->pageID=-1;
        newFrame->ref=0;
        newFrame->prox=NULL;
        aux=newFrame;
        processList[i]->head=newFrame; //<-----------
        for(j = 1; j < maxFrames; j++)
        {
            newFrame =(TFrame*)malloc(sizeof(TFrame));
            newFrame->frameID=j;
            newFrame->pageID=-1;
            newFrame->ref=0;
            newFrame->prox=NULL;
            aux->prox=newFrame;
            aux=newFrame;
        }
        aux->prox = processList[i]->head;//<--------------
    }
    //printf("memoria criada com sucesso\n");
    return 0;
}


int printMemory(TProcess *processList[], int num_procs, int maxFrames)
{
    //printf("printMemory\n");
    TFrame *aux;
    int i,j;
    for (i = 0; i < num_procs; i++)
    {
        aux=processList[i]->head;
        for(j = 0; j < maxFrames; j++)
        {
            printf("PID:%d\nFrame ID: %d\nPage ID:%d\n-----\n",i,aux->frameID,aux->pageID);
            aux=aux->prox;
        }
        //printf("Back to head: %d\n",aux->frameID);
        printf("========================\n");
    }
    return 0;
}


int startClockPointers(TFrame *clockPointer[],TProcess *processList[], int num_procs)
{
    //printf("startClockPointers\n");
    int i;

    for(i = 0; i < num_procs; i++)
    {
        clockPointer[i]=processList[i]->head;
    }

    //printf("vetor de processos criado com sucesso\n");
    return 0;
}

int seekPage(TProcess *processList[],int wantedProcess, int wantedPageID)
{
    //printf("seekPage\n");
    TFrame *aux = processList[wantedProcess]->head;
    do
    {
        if(aux->pageID == wantedPageID)
        {
            //printf("HIT\n");
            aux->ref=1;
            return 0;//HIT
        }
        aux=aux->prox;
    }
    while(aux != processList[wantedProcess]->head);
    //printf("MISS\n");
    return 1;
}

TFrame* insertOnMemory(TFrame *clockPointer,int wantedPageID)
{
    //printf("insertOnMemory\n");

    TFrame *aux = clockPointer;

    for(;;)
    {
        //printf("%d\n",aux->ref);
        //printf("checkpoint\n");
        if(aux->ref == 0) //VICTIM PAGE
        {
            //printf("VICTIM FOUND!\n");
            aux->pageID=wantedPageID;
            aux->ref=1;
            aux=aux->prox;
            //clockPointer=aux;
            return aux;
        }
        else
        {
            // printf("SECOND CHANCE GIVED\n");
            aux->ref = 0;
            aux=aux->prox;
        }
    }
}


void printResults(struct result * res,int num_procs)
{
    int i;
    printf("MEMVIRT.C\n");
    printf("Total de referencias por processo:\n");

    for(i=0; i<num_procs; i++)
    {
        printf("PID %d: %d\n",i,res->refs[i]);
    }
    printf("====\n");
    printf("Total de faults por processo:\n");
    for(i=0; i<num_procs; i++)
    {
        printf("PID %d: %d\n",i,res->pfs[i]);
        //res->pf_rate[i]=(((float) res->pfs[i] / res->refs[i]) * 100);
    }
    printf("Taxa de faults por processo:\n");
    for(i=0; i<num_procs; i++)
    {
        printf("PID %d: %.2f%% \n",i,res->pf_rate[i]);
    }
    printf("Taxa total de faults: %.2f%%\n",res->total_pf_rate);
    return;
}
